use atat::atat_derive::AtatEnum;

/// Echo on
#[derive(Debug, Clone, PartialEq, AtatEnum)]
#[repr(u8)]
pub enum EchoOn {
    ///  Unit does not echo the characters in command mode
    Off = 0,
    /// Unit echoes the characters in command mode. (default)
    On = 1,
}

/// Functionality level of UE
#[derive(Debug, Clone, PartialEq, AtatEnum)]
#[repr(u8)]
pub enum FunctionalityLevelOfUE {
    /// Minimum functionality
    Minimum = 0,
    /// Full functionality (default)
    Full = 1,
    /// Disable modem both transmit and receive RF circuits
    DisableRF = 4,
}

/// Configure RATs Searching Sequence effect
#[derive(Debug, Clone, PartialEq, AtatEnum)]
#[repr(u8)]
pub enum ConfigurationEffect {
    /// After reboot
    AfterReboot = 0,
    /// immediately
    Immediately = 1,
}

/// Echo on
#[derive(Debug, Clone, PartialEq, AtatEnum)]
#[repr(u8)]
pub enum PowerDownMode {
    ///  Immediately power down
    Immediate = 0,
    /// Normal power down (default)
    Normal = 1,
}
