use thiserror::Error;

pub mod cellular;
pub mod quectel_atat;

#[derive(Debug, Error)]
pub enum ModemError {
    #[error("Modem not responding")]
    NotResponding,
    #[error("SIM failure")]
    SimError,
    #[error("SIM unknown")]
    SimErrorUnknown,
    #[error("Not attached to network")]
    NoNetwork,
    #[error("No active context")]
    NoContext,
    #[error("MQTT error")]
    MqttRequestFailed,
    #[error("MQTT request failed")]
    NtpRequestFailed,
    #[error("NTP response not valid")]
    NotSupported,
    #[error("Request timeout")]
    OperationTimeout,
}
