// Tested with:
// BG95M3LAR02A03_01.012.01.012
// BG95M3LAR02A03_01.200.01.200

// To be tested with:
// BG95M3LAR02A03_01.014.01.014
// BG96MAR02A07M1G_01.018.01.018

use log::*;

use embedded_hal::digital::OutputPin;
use time;

use crate::quectel_atat::types::*;
use crate::quectel_atat::urc::Urc;
use crate::quectel_atat::*;

use atat::blocking::{AtatClient, Client};
use atat::heapless::String as HeaplessString;
use atat::heapless_bytes::Bytes as HeaplessBytes;
use atat::UrcChannel;

use crate::ModemError;

#[derive(Debug)]
pub enum ModemMode {
    EDGE,
    GPRS,
    NBIoT,
    LTEM,
    Unknown,
}

#[derive(Debug, PartialEq)]
enum ModemRevision {
    R200,
    R018,
    R014,
    R012,
    Unknown,
}

pub const INGRESS_BUF_SIZE: usize = 1024;
pub const URC_CAPACITY: usize = 128;
pub const URC_SUBSCRIBERS: usize = 3;

pub struct QuectelBG9X<W: embedded_io::Write, OutputPinGeneric: OutputPin> {
    pwr_key_pin: OutputPinGeneric,
    client: Client<'static, W, INGRESS_BUF_SIZE>,
    urc_channel: &'static UrcChannel<Urc, URC_CAPACITY, URC_SUBSCRIBERS>,
    imei: [u8; 15],
    mode: ModemMode,
    rev: ModemRevision,
}

impl<W: embedded_io::Write, OutputPinGeneric: OutputPin> QuectelBG9X<W, OutputPinGeneric> {
    pub fn new(
        power_gpio: OutputPinGeneric,
        client: Client<'static, W, INGRESS_BUF_SIZE>,
        urc_channel: &'static UrcChannel<Urc, URC_CAPACITY, URC_SUBSCRIBERS>,
    ) -> Result<Self, ModemError> {
        log::info!("Initializing Quectel BG9X modem");

        let mut driver = QuectelBG9X {
            pwr_key_pin: power_gpio,
            client,
            urc_channel,
            imei: [0; 15],
            mode: ModemMode::Unknown,
            rev: ModemRevision::Unknown,
        };

        driver.power_on()?;
        // driver.disable_echo()?;  // not necessary in atat
        driver.update_module_revision()?;
        driver.update_imei()?;

        Ok(driver)
    }

    pub fn set_mode(&mut self, mode: ModemMode) {
        self.mode = mode;
    }

    fn send_power_key(&mut self) {
        self.pwr_key_pin.set_high().unwrap();
        std::thread::sleep(std::time::Duration::from_millis(500));
        self.pwr_key_pin.set_low().unwrap();
    }

    fn update_module_revision(&mut self) -> Result<(), ModemError> {
        // TODO: this could go to a specific command parser
        // References:
        //   https://docs.rs/atat_derive/latest/atat_derive/derive.AtatCmd.html
        //
        match self.client.send(&GetVersionInfo) {
            Ok(version) => {
                log::info!("Modem version: {:?}", version);
                let version_code: &[u8] = version.code.as_slice();
                let version_str = std::str::from_utf8(version_code).unwrap();
                if version_str.contains("BG95M3LAR02A03_01.200.01.200") {
                    self.rev = ModemRevision::R200;
                } else if version_str.contains("BG96MAR02A07M1G_01.018.01.018") {
                    self.rev = ModemRevision::R018;
                } else if version_str.contains("BG95M3LAR02A03_01.014.01.014") {
                    self.rev = ModemRevision::R014;
                } else if version_str.contains("BG95M3LAR02A03_01.012.01.012") {
                    self.rev = ModemRevision::R012;
                } else {
                    self.rev = ModemRevision::Unknown;
                    log::warn!("Unknown modem revision")
                }
                Ok(())
            }
            Err(e) => {
                log::error!("Modem version not found: {:?}", e);
                Err(ModemError::NotResponding)
            }
        }
    }

    fn update_imei(&mut self) -> Result<(), ModemError> {
        let imei = match self.client.send(&GetImei) {
            Ok(imei) => imei.imei,
            Err(e) => {
                log::error!("IMEI not found: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        self.imei.copy_from_slice(imei.as_slice());
        let imei_str = std::str::from_utf8(&self.imei).unwrap();
        info!("IMEI: {}", imei_str);

        Ok(())
    }

    #[allow(dead_code)]
    fn disable_echo(&mut self) -> Result<(), ModemError> {
        match self.client.send(&SetEcho { on: EchoOn::Off }) {
            Ok(_) => {
                log::info!("Echo off");
                Ok(())
            }
            Err(e) => {
                log::error!("Echo off failed: {:?}", e);
                Err(ModemError::NotResponding)
            }
        }
    }

    fn is_powered_on(&mut self) -> Result<(), ModemError> {
        // TODO: deal with the start URC, that has this format:
        //pub const CMD_POWER_ON_RES: &[u8] = b"\r\nRDY\r\n\r\nAPP RDY\r\n";

        std::thread::sleep(std::time::Duration::from_secs(5));
        match self.client.send(&SetEcho { on: EchoOn::On }) {
            // With echo on, we can see the modem responses. Echo off also works.
            Ok(_) => {
                log::info!("Echo off");
            }
            Err(e) => {
                log::error!("Echo off failed: {:?}", e);
            }
        }

        // in the meantime, just wait for 1 second, send a command and check if it responds three times
        for _ in 0..3 {
            log::info!("Sending AT command");
            match self.client.send(&AT) {
                Ok(_) => {
                    log::info!("Response Ok");
                    return Ok(());
                }
                Err(e) => {
                    log::error!("Response failed: {:?}", e);
                }
            }
            std::thread::sleep(std::time::Duration::from_secs(5));
        }

        Err(ModemError::NotResponding)
    }

    fn is_powered_off(&mut self) -> Result<(), ModemError> {
        let mut subscriber = self.urc_channel.subscribe().unwrap();

        match self.client.send(&PowerDown {
            mode: PowerDownMode::Normal,
        }) {
            Ok(_) => {
                log::info!("Modem powering down");
            }
            Err(e) => {
                log::error!("Modem not powered down: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        }

        for _ in 0..10 {
            match subscriber.try_next_message_pure() {
                Some(Urc::PowerDown) => {
                    break;
                }
                _ => {
                    std::thread::sleep(std::time::Duration::from_secs(1));
                }
            }
        }

        // Double check if the modem is powered down
        match self.client.send(&AT) {
            Ok(_) => {
                return Err(ModemError::OperationTimeout);
            }
            Err(_) => {
                log::info!("Modem powered down");
                return Ok(());
            }
        }
    }

    pub fn is_alive(&mut self) -> Result<(), ModemError> {
        match self.client.send(&AT) {
            Ok(_) => {
                log::info!("Modem alive");
                Ok(())
            }
            Err(e) => {
                log::error!("Modem not alive: {:?}", e);
                Err(ModemError::NotResponding)
            }
        }
    }

    pub fn power_on(&mut self) -> Result<(), ModemError> {
        self.send_power_key();
        self.is_powered_on()?;

        Ok(())
    }

    pub fn power_off(&mut self) -> Result<(), ModemError> {
        self.is_powered_off()?;

        Ok(())
    }

    pub fn test_sim(&mut self) -> Result<(), ModemError> {
        let mut subscriber = self.urc_channel.subscribe().unwrap();

        match self.client.send(&GetSimStatus) {
            Ok(status) => {
                log::info!("SIM status: {:?}", status);
                let cmd_sim_ready = "READY";
                let cmd_sim_no_pin = "SIM PIN";
                if status.code.contains(cmd_sim_ready) {
                    log::info!("SIM Ready");
                    match self.client.send(&GetIccid {}) {
                        Ok(res) => {
                            log::info!("ICCID: {:?}", res.iccid);
                        }
                        Err(_) => {}
                    }
                    return Ok(());
                } else if status.code.contains(cmd_sim_no_pin) {
                    log::info!("PIN missing");
                    return Err(ModemError::SimError);
                }
            }
            Err(e) => {
                log::error!("Unknown SIM status response: {:?}", e);
                // Do not return error here, as we can still try to get the SIM status from URC
            }
        }

        for _ in 0..2 {
            std::thread::sleep(std::time::Duration::from_millis(1000));
            match subscriber.try_next_message_pure() {
                Some(Urc::CmeError(cme_error)) => {
                    log::error!("CME error: {:?}", cme_error);
                    match cme_error.err {
                        10 => {
                            log::error!("SIM not inserted");
                            return Err(ModemError::SimError);
                        }
                        11 => {
                            log::error!("SIM PIN required");
                            return Err(ModemError::SimError);
                        }
                        _ => {
                            log::error!("Unknown CME error: {:?}", cme_error);
                        }
                    }
                    return Err(ModemError::SimError);
                }
                Some(e) => {
                    log::error!("Unknown URC {:?}", e);
                }
                None => {
                    log::debug!("Waiting for response...");
                }
            }
        }

        Err(ModemError::SimErrorUnknown)
    }

    pub fn set_modem_funcionality(&mut self, on: bool) -> Result<(), ModemError> {
        match self.client.send(&SetUeFunctionality {
            fun: match on {
                true => FunctionalityLevelOfUE::Full,
                false => FunctionalityLevelOfUE::Minimum,
            },
        }) {
            Ok(_) => Ok(()),
            Err(e) => {
                log::error!("Modem functionality not set: {:?}", e);
                Err(ModemError::NotResponding)
            }
        }
    }

    /// Factory reset the modem
    ///
    /// This function should not be called very often because it erases the internal flash memory.
    pub fn factory_reset(&mut self) -> Result<(), ModemError> {
        warn!("Factory Reset. This function should not be called very often.");

        match self.client.send(&ResetToFactoryDefault {}) {
            Ok(_) => {}
            Err(e) => {
                log::error!("Factory reset not set: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        }

        match self.client.send(&RestoreFactoryConfiguration {}) {
            Ok(_) => Ok(()),
            Err(e) => {
                log::error!("Restore configuration failed: {:?}", e);
                Err(ModemError::NotResponding)
            }
        }
    }

    pub fn set_modem_configuration(&mut self) -> Result<(), ModemError> {
        #[cfg(feature = "bands_eu")]
        match self.client.send(&ConfigureBandsEurope {}) {
            Ok(_) => {
                log::warn!("Modem bands set to Europe");
            }
            Err(e) => {
                log::error!("Modem configuration not set: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        match self.client.send(&ConfigureRatSearchingSequence {
            param: HeaplessString::try_from("nwscanseq").unwrap(),
            rat_searching_sequence: HeaplessBytes::from_slice(b"0201").unwrap(), // LTE-M -> 2G
            effect: ConfigurationEffect::Immediately,
        }) {
            Ok(_) => {}
            Err(e) => {
                log::error!("Modem configuration not set: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        match self.client.send(&ConfigureRatSearchingMode {
            param: HeaplessString::try_from("nwscanmode").unwrap(),
            rat_searching_mode: 0, // Automatic: GSM and LTE
            effect: ConfigurationEffect::Immediately,
        }) {
            Ok(_) => {}
            Err(e) => {
                log::error!("Modem configuration not set: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        match self.client.send(&ConfigureServiceDomain {
            param: HeaplessString::try_from("servicedomain").unwrap(),
            service_domain: 1, // PS: Packet Switched
            effect: ConfigurationEffect::Immediately,
        }) {
            Ok(_) => {}
            Err(e) => {
                log::error!("Modem configuration not set: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        match self.client.send(&ConfigureIotOpMode {
            param: HeaplessString::try_from("iotopmode").unwrap(),
            mode: 0, // eMTC
            effect: ConfigurationEffect::Immediately,
        }) {
            Ok(_) => {}
            Err(e) => {
                log::error!("Modem configuration not set: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        log::info!("Modem configuration set");
        Ok(())
    }

    pub fn get_nitz_time(&mut self) -> Result<i64, ModemError> {
        match self.client.send(&GetNetworkNitzTime { mode: 1 }) {
            Ok(network_time_info) => {
                // Turn "1970/01/01,00:00:00+00,0" into "1970-01-01T00:00:00Z"
                log::info!("Network time: {:?}", network_time_info);
                // Ignore timezone (indicates the difference, expressed in quarters of an hour, between the local time and GMT)
                // Ignore Daylight saving time
                let dt_str = network_time_info.time_and_dst.as_str();
                let fd = time::format_description::parse(
                    "[year]/[month]/[day],[hour]:[minute]:[second]+[ignore count:2],[ignore count:1]",
                )
                .unwrap();
                let dt = time::PrimitiveDateTime::parse(dt_str, &fd).unwrap();
                let dt = dt.assume_offset(time::UtcOffset::UTC);

                let ts = dt.unix_timestamp();
                log::info!("Timestamp: {}", ts);

                return Ok(ts);
            }
            Err(e) => {
                log::error!("Network time not found: {:?}", e);
                Err(ModemError::NotResponding)
            }
        }
    }

    pub fn get_ntp_time(&mut self, ntp_server: &str) -> Result<i64, ModemError> {
        match self.client.send(&GetNetworkNtpTime {
            context_id: 1,
            server: HeaplessString::try_from(ntp_server).unwrap(),
        }) {
            Ok(_) => {
                log::info!("NTP request sent");
            }
            Err(e) => {
                log::error!("Network time not found: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        }

        let mut subscriber = self.urc_channel.subscribe().unwrap();
        let now = std::time::SystemTime::now();
        while now.elapsed().unwrap() < std::time::Duration::from_secs(10) {
            std::thread::sleep(std::time::Duration::from_millis(500));

            match subscriber.try_next_message_pure() {
                Some(Urc::NtpTime(res)) => {
                    match res.err {
                        0 => {}
                        _ => {
                            log::error!("NTP failed");
                            return Err(ModemError::NtpRequestFailed);
                        }
                    }

                    // Turn "1970/01/01,00:00:00+00,0" into "1970-01-01T00:00:00Z"
                    log::info!("Network time: {:?}", res.time);
                    // Ignore timezone (indicates the difference, expressed in quarters of an hour, between the local time and GMT)
                    // Ignore Daylight saving time
                    let dt_str = res.time.as_str();
                    let fd = time::format_description::parse(
                        "[year]/[month]/[day],[hour]:[minute]:[second]+[ignore count:2]",
                    )
                    .unwrap();
                    let dt = time::PrimitiveDateTime::parse(dt_str, &fd).unwrap();
                    let dt = dt.assume_offset(time::UtcOffset::UTC);

                    let ts = dt.unix_timestamp();
                    log::info!("Timestamp: {}", ts);

                    return Ok(ts);
                }
                Some(e) => {
                    log::error!("Unknown URC {:?}", e);
                }
                None => {
                    log::debug!("Waiting for response...");
                }
            }
        }

        Err(ModemError::NotResponding)
    }

    pub fn get_signal_strength(&mut self) -> Result<(i16, u8), ModemError> {
        match self.client.send(&GetSignalStrength) {
            Ok(signal_strength) => {
                if let Some(rssi) = signal_strength.rssi {
                    let signal = if rssi > -30 {
                        -30
                    } else if rssi < -140 {
                        -140
                    } else {
                        rssi
                    };
                    let signal = -100 * (signal + 140) / (-140 + 30);
                    log::info!("RSSI: {}dB ({}%)", rssi, signal);
                    Ok((rssi, signal as u8))
                } else {
                    Err(ModemError::NoNetwork)
                }
            }
            Err(e) => {
                log::error!("Signal strength not found: {:?}", e);
                Err(ModemError::NotResponding)
            }
        }
    }

    fn register_gprs(
        &mut self,
        timeout: std::time::Duration,
    ) -> Result<std::time::Duration, ModemError> {
        let now = std::time::SystemTime::now();

        while now.elapsed().unwrap() < timeout {
            std::thread::sleep(std::time::Duration::from_millis(500));

            match self.client.send(&GetEGPRSNetworkRegistrationStatus {}) {
                Ok(status) => {
                    log::info!("GPRS network registration status: {:?}", status);
                    match status.stat {
                        1 => {
                            let t = now.elapsed().unwrap();
                            log::info!("Registered (Home) after {} s", t.as_secs());
                            return Ok(t);
                        }
                        2 => {
                            print!("."); // Searching
                            continue;
                        }
                        3 => {
                            log::error!("Registration denied");
                            return Err(ModemError::NoNetwork);
                        }
                        4 => {
                            log::error!("Registration failed");
                            return Err(ModemError::NoNetwork);
                        }
                        5 => {
                            let t = now.elapsed().unwrap();
                            log::info!("Registered (Roaming) after {} s", t.as_secs());
                            return Ok(t);
                        }
                        _ => {
                            log::error!("Unknown registration status");
                            return Err(ModemError::NoNetwork);
                        }
                    }
                }
                Err(e) => {
                    log::error!("GPRS network registration status not found: {:?}", e);
                }
            }
        }

        Err(ModemError::OperationTimeout)
    }

    fn register_eps(
        &mut self,
        timeout: std::time::Duration,
    ) -> Result<std::time::Duration, ModemError> {
        let now = std::time::SystemTime::now();

        while now.elapsed().unwrap() < timeout {
            std::thread::sleep(std::time::Duration::from_millis(500));

            match self.client.send(&GetEPSNetworkRegistrationStatus {}) {
                Ok(status) => {
                    log::info!("EPS network registration status: {:?}", status);
                    match status.stat {
                        1 => {
                            let t = now.elapsed().unwrap();
                            log::info!("Registered (Home) after {} s", t.as_secs());
                            return Ok(t);
                        }
                        2 => {
                            print!("."); // Searching
                            continue;
                        }
                        3 => {
                            log::error!("Registration denied");
                            return Err(ModemError::NoNetwork);
                        }
                        4 => {
                            log::error!("Registration failed");
                            return Err(ModemError::NoNetwork);
                        }
                        5 => {
                            let t = now.elapsed().unwrap();
                            log::info!("Registered (Roaming) after {} s", t.as_secs());
                            return Ok(t);
                        }
                        _ => {
                            log::error!("Unknown registration status");
                            return Err(ModemError::NoNetwork);
                        }
                    }
                }
                Err(e) => {
                    log::error!("EPS network registration status not found: {:?}", e);
                }
            }
        }

        Err(ModemError::OperationTimeout)
    }

    fn network_attach_info(
        &mut self,
        timeout: std::time::Duration,
    ) -> Result<std::time::Duration, ModemError> {
        let now = std::time::SystemTime::now();

        info!("Attaching...");
        while now.elapsed().unwrap() < timeout {
            std::thread::sleep(std::time::Duration::from_millis(500));

            match self.client.send(&GetNetworkInfo) {
                Ok(info) => {
                    log::info!("Network info: {:?}", info);
                    if info.act.contains("No Service") {
                        print!(".");
                        continue;
                    } else if info.act.contains("EDGE") {
                        self.mode = ModemMode::EDGE;
                        let t = now.elapsed().unwrap();
                        log::info!("Using 2G after {} s", t.as_secs());
                        return Ok(t);
                    } else if info.act.contains("GPRS") {
                        self.mode = ModemMode::GPRS;
                        let t = now.elapsed().unwrap();
                        log::info!("Using 2G after {} s", t.as_secs());
                        return Ok(t);
                    } else if info.act.contains("eMTC") || info.act.contains("CAT-M1") {
                        self.mode = ModemMode::LTEM;
                        let t = now.elapsed().unwrap();
                        log::info!("Using LTE-M after {} s", t.as_secs());
                        return Ok(t);
                    } else if info.act.contains("NBIoT") || info.act.contains("CAT-NB1") {
                        self.mode = ModemMode::NBIoT;
                        let t = now.elapsed().unwrap();
                        log::info!("Using NB-IoT after {} s", t.as_secs());
                        return Ok(t);
                    }
                }
                Err(e) => {
                    log::error!("Network info not found: {:?}", e);
                }
            }
        }

        Err(ModemError::OperationTimeout)
    }

    pub fn network_attach(&mut self) -> Result<std::time::Duration, ModemError> {
        let timeout = std::time::Duration::from_secs(60);

        let attach_duration = self.network_attach_info(timeout)?;
        let reg_duration = match self.mode {
            ModemMode::EDGE => self.register_gprs(timeout - attach_duration),
            ModemMode::GPRS => self.register_gprs(timeout - attach_duration),
            ModemMode::LTEM => self.register_eps(timeout - attach_duration),
            ModemMode::NBIoT => self.register_eps(timeout - attach_duration),
            ModemMode::Unknown => Err(ModemError::NoNetwork),
        }?;

        Ok(attach_duration + reg_duration)
    }

    pub fn set_context_configuration(
        &mut self,
        modem_apn: &str,
        modem_user: &str,
        modem_pass: &str,
    ) -> Result<(), ModemError> {
        info!("Configuring context...");

        match self.client.send(&ConfigureContext {
            context_id: 1,
            context_type: 1, // IPV4
            apn: HeaplessString::try_from(modem_apn).unwrap(),
            username: HeaplessString::try_from(modem_user).unwrap(),
            password: HeaplessString::try_from(modem_pass).unwrap(),
            authentication: 1, // PAP
        }) {
            Ok(_) => {
                log::info!("Context configuration set");
                Ok(())
            }
            Err(e) => {
                log::error!("Context configuration not set: {:?}", e);
                Err(ModemError::NotResponding)
            }
        }
    }

    pub fn context_activate(&mut self) -> Result<std::time::Duration, ModemError> {
        info!("Activating context...");

        let now = std::time::SystemTime::now();

        match self.client.send(&ActivatePDPContext { context_id: 1 }) {
            Ok(_) => {
                log::info!("Context activated");
            }
            Err(e) => {
                log::error!("Context not activated: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        }

        match self.client.send(&GetPDPContextInfo {}) {
            Ok(status) => {
                log::info!("Context status: {:?}", status);
                let t = now.elapsed().unwrap();
                log::info!(
                    "IP {:?} obtained after {} s",
                    status.ip_address,
                    t.as_secs()
                );
                Ok(t)
            }
            Err(e) => {
                log::error!("Context status not found: {:?}", e);
                Err(ModemError::NoContext)
            }
        }
    }

    pub fn context_deactivate(&mut self) -> Result<(), ModemError> {
        info!("Deactivating context...");

        match self.client.send(&DeactivatePDPContext { context_id: 1 }) {
            Ok(_) => {
                log::info!("Context deactivated");
                Ok(())
            }
            Err(e) => {
                log::error!("Context not deactivated: {:?}", e);
                Err(ModemError::NotResponding)
            }
        }
    }

    pub fn mqtt_connect(
        &mut self,
        url: &str,
        port: u16,
        id: &str,
        user: &str,
        pass: &str,
    ) -> Result<(), ModemError> {
        info!("Connecting to MQTT broker...");

        let mut subscriber = self.urc_channel.subscribe().unwrap();

        match self.client.send(&MqttOpen {
            link_id: 0,
            server: HeaplessString::try_from(url).unwrap(),
            port,
        }) {
            Ok(_) => {
                log::info!("Connected to MQTT broker");
            }
            Err(e) => {
                log::error!("MQTT broker not connected: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        }

        let now = std::time::SystemTime::now();
        while now.elapsed().unwrap() < std::time::Duration::from_secs(10) {
            std::thread::sleep(std::time::Duration::from_millis(500));

            match subscriber.try_next_message_pure() {
                Some(Urc::MqttOpen(mqtopen_response)) => {
                    log::info!("MQTT Open response: {:?}", mqtopen_response);
                    match mqtopen_response.result {
                        0 => {
                            log::info!("Connection opened");
                            break;
                        }
                        _ => {
                            log::error!("MQTT Open failed");
                            return Err(ModemError::MqttRequestFailed);
                        }
                    }
                }
                Some(e) => {
                    log::error!("Unknown URC {:?}", e);
                }
                None => {
                    log::debug!("Waiting for response...");
                }
            }
        }

        match self.client.send(&MqttConnect {
            tcp_connect_id: 0,
            client_id: HeaplessString::try_from(id).unwrap(),
            username: Some(HeaplessString::try_from(user).unwrap()),
            password: Some(HeaplessString::try_from(pass).unwrap()),
        }) {
            Ok(_) => {
                log::info!("Connected to MQTT broker");
            }
            Err(e) => {
                log::error!("MQTT broker not connected: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        }

        let now = std::time::SystemTime::now();
        while now.elapsed().unwrap() < std::time::Duration::from_secs(5) {
            std::thread::sleep(std::time::Duration::from_millis(500));
            match subscriber.try_next_message_pure() {
                Some(Urc::MqttConnect(mqtconnect_response)) => {
                    log::info!("MQTT Connect response: {:?}", mqtconnect_response);
                    match mqtconnect_response.result {
                        0 => {
                            log::info!("Client connected");
                            break;
                        }
                        _ => {
                            log::error!("MQTT Connect failed");
                            return Err(ModemError::MqttRequestFailed);
                        }
                    }
                }
                Some(e) => {
                    log::error!("Unknown URC {:?}", e);
                }
                None => {
                    log::debug!("Waiting for response...");
                }
            }
        }
        Ok(())
    }

    pub fn mqtt_disconnect(&mut self) -> Result<(), ModemError> {
        info!("Disconnecting from MQTT broker...");

        let mut subscriber = self.urc_channel.subscribe().unwrap();

        match self.client.send(&MqttDisconnect { tcp_connect_id: 0 }) {
            Ok(_) => {
                log::info!("Disconnected from MQTT broker");
            }
            Err(e) => {
                log::error!("MQTT broker not disconnected: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        }

        let now = std::time::SystemTime::now();
        while now.elapsed().unwrap() < std::time::Duration::from_secs(10) {
            std::thread::sleep(std::time::Duration::from_millis(500));
            match subscriber.try_next_message_pure() {
                Some(Urc::MqttDisconnect(mqtdisconnect_response)) => {
                    log::info!("MQTT Disconnect response: {:?}", mqtdisconnect_response);
                    match mqtdisconnect_response.result {
                        0 => {
                            log::info!("Client disconnected");
                            break;
                        }
                        _ => {
                            log::error!("MQTT Disconnect failed");
                            return Err(ModemError::MqttRequestFailed);
                        }
                    }
                }
                Some(e) => {
                    log::error!("Unknown URC {:?}", e);
                }
                None => {
                    log::debug!("Waiting for response...");
                }
            }
        }

        if self.rev == ModemRevision::R200 {
            let now = std::time::SystemTime::now();
            while now.elapsed().unwrap() < std::time::Duration::from_secs(5) {
                std::thread::sleep(std::time::Duration::from_millis(500));
                match subscriber.try_next_message_pure() {
                    Some(Urc::MqttStatus(mqttstatus_response)) => {
                        log::info!("MQTT Status response: {:?}", mqttstatus_response);
                        match mqttstatus_response.err {
                            5 => {
                                log::info!("Client disconnected");
                                return Ok(());
                            }
                            _ => {
                                log::error!("MQTT Status failed");
                                return Err(ModemError::MqttRequestFailed);
                            }
                        }
                    }
                    Some(e) => {
                        log::error!("Unknown URC {:?}", e);
                    }
                    None => {
                        log::debug!("Waiting for response...");
                    }
                }
            }

            match self.client.send(&MqttClose { tcp_connect_id: 0 }) {
                Ok(_) => {
                    log::info!("Disconnected from MQTT broker");
                }
                Err(e) => {
                    log::error!("MQTT broker not disconnected: {:?}", e);
                    return Err(ModemError::NotResponding);
                }
            }

            let now = std::time::SystemTime::now();
            while now.elapsed().unwrap() < std::time::Duration::from_secs(5) {
                std::thread::sleep(std::time::Duration::from_millis(500));
                match subscriber.try_next_message_pure() {
                    Some(Urc::MqttClose(mqtclose_response)) => {
                        log::info!("MQTT Close response: {:?}", mqtclose_response);
                        match mqtclose_response.result {
                            0 => {
                                log::info!("Connection closed");
                                return Ok(());
                            }
                            _ => {
                                log::error!("MQTT Close failed");
                                return Err(ModemError::MqttRequestFailed);
                            }
                        }
                    }
                    Some(e) => {
                        log::error!("Unknown URC {:?}", e);
                    }
                    None => {
                        log::debug!("Waiting for response...");
                    }
                }
            }
        }

        Ok(())
    }

    pub fn mqtt_publish(&mut self, topic: &str, payload: &str, qos: u8) -> Result<(), ModemError> {
        info!("Publishing to {}", topic);
        let msg_id = if qos == 0 { 0 } else { 1 };

        let mut subscriber = self.urc_channel.subscribe().unwrap();

        match self.client.send(&MqttPublishExtended {
            tcp_connect_id: 0,
            msg_id,
            qos,
            retain: 0,
            topic: HeaplessString::try_from(topic).unwrap(),
            payload: HeaplessString::try_from(payload).unwrap(),
        }) {
            Ok(_) => {
                log::info!("Published to MQTT broker");
            }
            Err(e) => {
                log::error!("MQTT broker not published: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        }

        let now = std::time::SystemTime::now();
        while now.elapsed().unwrap() < std::time::Duration::from_secs(10) {
            std::thread::sleep(std::time::Duration::from_millis(500));
            match subscriber.try_next_message_pure() {
                Some(Urc::MqttPublish(mqtpublish_response)) => {
                    log::info!("MQTT Publish response: {:?}", mqtpublish_response);
                    match mqtpublish_response.result {
                        0 => {
                            log::info!("Publishing successful");
                            break;
                        }
                        _ => {
                            log::error!("Publishing failed");
                            return Err(ModemError::MqttRequestFailed);
                        }
                    }
                }
                Some(e) => {
                    log::error!("Unknown URC {:?}", e);
                }
                None => {
                    log::debug!("Waiting for response...");
                }
            }
        }

        Ok(())
    }
}
