// Test Quectel On
use std::{thread, time};

use esp_idf_sys as _; // If using the `binstart` feature of `esp-idf-sys`, always keep this module imported

use esp_idf_hal::delay;
use esp_idf_hal::gpio;
use esp_idf_hal::prelude::*;
use esp_idf_hal::uart;
use esp_idf_svc::log::EspLogger;

use log::*;

use atat::blocking::Client;
use atat::AtatIngress;
use atat::DefaultDigester;
use atat::Ingress;
use atat::{Config as AtatConfig, ResponseSlot, UrcChannel};
use quectel_bg9x_eh_driver::cellular::QuectelBG9X;
use quectel_bg9x_eh_driver::cellular::INGRESS_BUF_SIZE;
use quectel_bg9x_eh_driver::cellular::URC_CAPACITY;
use quectel_bg9x_eh_driver::cellular::URC_SUBSCRIBERS;
use quectel_bg9x_eh_driver::quectel_atat::urc::Urc;
use static_cell::StaticCell;

#[toml_cfg::toml_config]
struct Config {
    #[default("")]
    modem_apn: &'static str,
    #[default("")]
    modem_user: &'static str,
    #[default("")]
    modem_pass: &'static str,

    #[default("test.mosquitto.org")]
    mqtt_server: &'static str,
    #[default(1883)]
    mqtt_port: u16,
    #[default("")]
    mqtt_user: &'static str,
    #[default("")]
    mqtt_pass: &'static str,
}

fn main() {
    // Temporary. Will disappear once ESP-IDF 4.4 is released, but for now it is necessary to call this function once,
    // or else some patches to the runtime implemented by esp-idf-sys might not link properly.
    esp_idf_sys::link_patches();
    EspLogger::initialize_default();

    let peripherals = Peripherals::take().unwrap();

    info!("Testing Quectel BG9X");
    let mut modem_pwr_en = gpio::PinDriver::output(peripherals.pins.gpio3).unwrap();
    let modem_pwr_key = gpio::PinDriver::output(peripherals.pins.gpio2).unwrap();
    let modem_tx = peripherals.pins.gpio0;
    let modem_rx = peripherals.pins.gpio1;
    let uart_conf = uart::UartConfig::new()
        .baudrate(115_200.Hz())
        .source_clock(uart::config::SourceClock::RTC);
    let uart = uart::UartDriver::new(
        peripherals.uart1,
        modem_tx,
        modem_rx,
        Option::<gpio::AnyIOPin>::None,
        Option::<gpio::AnyIOPin>::None,
        &uart_conf,
    )
    .unwrap();

    // Make sure modem starts disabled
    modem_pwr_en.set_low().unwrap();
    thread::sleep(time::Duration::from_millis(500));

    // Enable modem back
    modem_pwr_en.set_high().unwrap();

    // atat init
    static INGRESS_BUF: StaticCell<[u8; INGRESS_BUF_SIZE]> = StaticCell::new();
    static RES_SLOT: ResponseSlot<INGRESS_BUF_SIZE> = ResponseSlot::new();
    static URC_CHANNEL: UrcChannel<Urc, URC_CAPACITY, URC_SUBSCRIBERS> = UrcChannel::new();
    let mut ingress = Ingress::new(
        DefaultDigester::<Urc>::default(),
        INGRESS_BUF.init([0; INGRESS_BUF_SIZE]),
        &RES_SLOT,
        &URC_CHANNEL,
    );

    static BUF: StaticCell<[u8; 1024]> = StaticCell::new();
    let buf = BUF.init([0; 1024]);

    let (tx, rx) = uart.into_split();
    let client = Client::new(tx, &RES_SLOT, buf, AtatConfig::default());

    let builder = thread::Builder::new().stack_size(8 * 1024);
    let _ = builder.spawn(move || loop {
        let buf = ingress.write_buf();
        match rx.read(
            buf,
            delay::TickType::from(time::Duration::from_millis(50)).0,
        ) {
            Ok(len) => {
                if len != 0 {
                    let s = std::str::from_utf8(&buf[..len]).unwrap();
                    log::info!("Read: ({}) {}", len, s);
                }
                match ingress.try_advance(len) {
                    Ok(_) => {}
                    Err(e) => {
                        log::info!("Error advancing ingress {:?}", e);
                        ingress.clear();
                    }
                }
            }
            Err(e) => {
                // log::info!("Error reading from UART {:?}", e);
                ingress.clear();
            }
        }
    });
    // end of atat initialization

    let mut mm = match QuectelBG9X::new(modem_pwr_key, client, &URC_CHANNEL) {
        Ok(mm) => mm,
        Err(e) => {
            error!("Error initializing modem: {:?}", e);
            loop {
                thread::sleep(time::Duration::from_millis(1000));
            }
        }
    };
    mm.is_alive().unwrap();
    mm.test_sim().unwrap();
    mm.set_modem_funcionality(false).unwrap();
    // mm.factory_reset().unwrap();
    mm.set_modem_configuration().unwrap();
    mm.set_modem_funcionality(true).unwrap();
    mm.network_attach().unwrap();
    let (_, signalq) = mm.get_signal_strength().unwrap();
    info!("Signal quality: {}%", signalq);
    // According to Quectel, context configuration may be set before registering to the network
    // in case of registration failure
    mm.set_context_configuration(CONFIG.modem_apn, CONFIG.modem_user, CONFIG.modem_pass)
        .unwrap();

    let ts_nitz = mm.get_nitz_time();
    mm.context_activate().unwrap();
    let ts_ntp = mm.get_ntp_time("0.es.pool.ntp.org");

    info!("NITZ: {:?}", ts_nitz);
    info!("NTP: {:?}", ts_ntp);

    mm.mqtt_connect(
        CONFIG.mqtt_server,
        CONFIG.mqtt_port,
        "a_name",
        CONFIG.mqtt_user,
        CONFIG.mqtt_pass,
    )
    .unwrap();
    mm.mqtt_publish("some-test/", "some-payload", 0).unwrap();
    mm.mqtt_disconnect().unwrap();

    mm.context_deactivate().unwrap();
    mm.power_off().unwrap();

    modem_pwr_en.set_low().unwrap();

    loop {
        thread::sleep(time::Duration::from_millis(1000)); // So the background task does not starve
    }
}
