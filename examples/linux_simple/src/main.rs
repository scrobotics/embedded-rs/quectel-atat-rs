use std::{thread, time};

use quectel_bg9x_eh_driver::cellular::QuectelBG9X;
use quectel_bg9x_eh_driver::cellular::INGRESS_BUF_SIZE;
use quectel_bg9x_eh_driver::cellular::URC_CAPACITY;
use quectel_bg9x_eh_driver::cellular::URC_SUBSCRIBERS;
use quectel_bg9x_eh_driver::quectel_atat::urc::Urc;

use atat::blocking::Client;
use atat::AtatIngress;
use atat::DefaultDigester;
use atat::Ingress;
use atat::{Config as AtatConfig, ResponseSlot, UrcChannel};

use embedded_hal_mock::eh1::digital::{
    Mock as PinMock, State as PinState, Transaction as PinTransaction,
};
use embedded_io::Read;
use serialport;
use static_cell::StaticCell;

#[toml_cfg::toml_config]
struct Config {
    #[default("")]
    modem_apn: &'static str,
    #[default("")]
    modem_user: &'static str,
    #[default("")]
    modem_pass: &'static str,

    #[default("test.mosquitto.org")]
    mqtt_server: &'static str,
    #[default(1883)]
    mqtt_port: u16,
    #[default("")]
    mqtt_user: &'static str,
    #[default("")]
    mqtt_pass: &'static str,
}

fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Debug)
        .init();

    // Configure expectations
    let expectations = [
        PinTransaction::set(PinState::High),
        PinTransaction::set(PinState::Low),
    ];
    // Create pin
    let mut modem_pwr_key = PinMock::new(&expectations);

    // Open serial port
    let serial_tx = serialport::new("/dev/ttyUSB4", 115_200)
        .timeout(std::time::Duration::from_millis(1000))
        .open()
        .expect("Could not open serial port");
    let serial_rx = serial_tx.try_clone().expect("Could not clone serial port");

    let serial_tx = embedded_io_adapters::std::FromStd::new(serial_tx);
    let mut serial_rx = embedded_io_adapters::std::FromStd::new(serial_rx);

    static INGRESS_BUF: StaticCell<[u8; INGRESS_BUF_SIZE]> = StaticCell::new();
    static RES_SLOT: ResponseSlot<INGRESS_BUF_SIZE> = ResponseSlot::new();
    static URC_CHANNEL: UrcChannel<Urc, URC_CAPACITY, URC_SUBSCRIBERS> = UrcChannel::new();
    let mut ingress = Ingress::new(
        DefaultDigester::<Urc>::default(),
        INGRESS_BUF.init([0; INGRESS_BUF_SIZE]),
        &RES_SLOT,
        &URC_CHANNEL,
    );

    static BUF: StaticCell<[u8; 1024]> = StaticCell::new();
    let buf = BUF.init([0; 1024]);

    let client = Client::new(serial_tx, &RES_SLOT, buf, AtatConfig::default());

    log::info!("Starting ATAT loop...");
    let _ = std::thread::spawn(move || loop {
        let buf = ingress.write_buf();
        match serial_rx.read(buf) {
            Ok(len) => {
                if len != 0 {
                    // let s = std::str::from_utf8(&buf[..len]).unwrap();
                    // log::debug!("Read: ({}) {}", len, s);
                }
                match ingress.try_advance(len) {
                    Ok(_) => {}
                    Err(e) => {
                        log::info!("Error advancing ingress {:?}", e);
                        ingress.clear();
                    }
                }
            }
            Err(e) => {
                log::info!("Error reading from UART: {:?}", e);
                ingress.clear();
            }
        }
    });
    // end of atat initialization

    log::info!("Starting ATAT client...");
    let mut mm = match QuectelBG9X::new(modem_pwr_key.clone(), client, &URC_CHANNEL) {
        Ok(mm) => mm,
        Err(e) => {
            log::error!("Error initializing modem: {:?}", e);
            loop {
                thread::sleep(time::Duration::from_millis(1000));
            }
        }
    };
    mm.is_alive().unwrap();
    mm.test_sim().unwrap();
    mm.set_modem_funcionality(false).unwrap();
    // mm.factory_reset().unwrap();
    mm.set_modem_configuration().unwrap();
    mm.set_modem_funcionality(true).unwrap();

    mm.network_attach().unwrap();
    mm.set_context_configuration(CONFIG.modem_apn, CONFIG.modem_user, CONFIG.modem_pass)
        .unwrap();
    let _ = mm.get_signal_strength().unwrap();

    let _ = mm.get_nitz_time();
    mm.context_activate().unwrap();
    let _ = mm.get_ntp_time("0.es.pool.ntp.org");

    mm.mqtt_connect(
        CONFIG.mqtt_server,
        CONFIG.mqtt_port,
        "a_name",
        CONFIG.mqtt_user,
        CONFIG.mqtt_pass,
    )
    .unwrap();
    mm.mqtt_publish("some-test/", "some-payload", 0).unwrap();
    mm.mqtt_disconnect().unwrap();

    mm.context_deactivate().unwrap();
    mm.power_off().unwrap();

    modem_pwr_key.done();
}
