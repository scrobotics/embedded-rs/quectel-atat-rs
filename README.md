# AT Driver for the Quectel BG9X family of modems

This respository contains a driver for the [Quectel](https://www.quectel.com/) BG95/BG96 modems. The driver is built on top of the [atat crate](https://docs.rs/atat/latest/atat/).

The main features are:

* Send a PWR_KEY which turns on/off the module by toggling a GPIO
* Connect to an MQTT broker and publish data
* Uses LTE-M with fallback to 2G

The crate is available on [crates.io](https://crates.io/crates/quectel-bg9x-eh-driver).

## TODO
- [ ] Remove STD dependencies 
- [ ] Make modem user configurable (AT+QCFG commands)

## Examples

* `linux-simple`: example of interacting with the module connected through a serial converter to a Linux computer. Requires the converter to be `/dev/ttyUSB4` and the module to be always on (the PWR_KEY is faked).
    ```
    cd examples/linux_simple
    cargo run
    ```
* `esp32c3_connect_and_send_mqtt`: run the example in a board such us [Dark Sky Meter Hardware](https://gitlab.com/scrobotics/optical-makerspace/dark-sky-meter-hw)
    ```
    cd examples/esp32c3_connect_and_send_mqtt
    cargo run
    ```

For a more complete example visit Tested with the [Dark Sky Meter Firmware](https://gitlab.com/scrobotics/optical-makerspace/dark-sky-meter-fw).

## License
[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://gitlab.com/scrobotics/embedded-rs/adxl234-rs/-/blob/master/LICENSE)

This tool is released under the MIT license, hence allowing commercial use of the library. Please refer to the [LICENSE](https://gitlab.com/scrobotics/embedded-rs/adxl234-rs/-/blob/master/LICENSE) file.
